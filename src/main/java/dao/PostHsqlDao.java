package dao;

import model.Post;
import model.PostItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Primary
@Repository
public class PostHsqlDao implements PostDao {

    @Autowired
    private JdbcTemplate template;


    @Override
    public Post save(Post post) {

        String sql = "INSERT INTO POST" +
                "(id, orderNumber)"+
                "VALUES (next value for seq2, ?)";

        GeneratedKeyHolder holder = new GeneratedKeyHolder();

        template.update(conn -> {

            PreparedStatement ps = conn.prepareStatement(
                    sql, new String[] {"id"});

            ps.setString(1, post.getOrderNumber());
            return ps;

        }, holder);

        post.setId(holder.getKey().longValue());


        //  INSERT INTO POSTITEM
        List<PostItem> postItems = post.getOrderRows();

        if(postItems != null) {
            String sqlPostItem = "INSERT INTO POSTITEM" +
                    "(id, itemName, quantity, price, postID)" +
                    "VALUES (next value for seq1, ?, ?, ?, ?)";


            Iterator<PostItem> postItemIterator = postItems.iterator();


            while (postItemIterator.hasNext()) {
                PostItem postItem = postItemIterator.next();

                template.update(conn -> {

                    PreparedStatement ps = conn.prepareStatement(
                            sqlPostItem, new String[] {"id"});

                    ps.setString(1, postItem.getItemName());
                    ps.setInt(2, postItem.getQuantity());
                    ps.setInt(3, postItem.getPrice());
                    ps.setLong(4, post.getId());
                    return ps;
                }, holder);

            }
        }
        System.out.println("SAVED: "+post);
        return post;
    }



    @Override
    public List<Post> getOrderList() {


        List<Post> postItemList = new ArrayList<Post>();

        String sql = "SELECT * FROM POST";

        List<Post> rows = template.query(sql, new BeanPropertyRowMapper(Post.class));


        for (Post row : rows) {
            Post post = new Post(
                    row.getId(),
                    row.getOrderNumber(),
                    row.getOrderRows()
                );
            postItemList.add(post);
        }
        return postItemList;
    }

    @Override
    public Post getOrderById(Long id){
        Post post = new Post();
        String sql = "SELECT * FROM POST WHERE id="+ String.valueOf(id);
        List<Post> rows = template.query(sql, new BeanPropertyRowMapper(Post.class));

        for (Post row : rows) {
             post = new Post(
                    row.getId(),
                    row.getOrderNumber(),
                    row.getOrderRows()
            );
        }
        return post;
    }

    @Override
    public void delete(Long id) {
        String sql = "delete from post where id = ?";

        template.update(sql, id);
    }

    /*


    @Override
    public Post findById(Long id) {
        String sql = "select id, title, text "
                   + "from post where id = ?";

        return template.queryForObject(sql, new Object[] {id}, getPostRowMapper());
    }

    private RowMapper<Post> getPostRowMapper() {
        return (rs, rowNum) -> new Post(
                    rs.getLong("id"),
                    rs.getString("title"),
                    rs.getString("text"));
    }
    */

}
