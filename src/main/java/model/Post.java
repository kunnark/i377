package model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class Post {
    private Long id;

    @NotNull
    @Size(min=2, max=10)
    private String orderNumber;

    @Valid
    private List<PostItem> orderRows;

    public Post(Long id, String orderNumber, List<PostItem> orderRows){
        this.id = id;
        this.orderNumber = orderNumber;
        this.orderRows = orderRows;
    }

    public Post(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNumber() { return orderNumber; }

    public void setOrderNumber(String orderNumber) { this.orderNumber = orderNumber; }

    public List<PostItem> getOrderRows(){
        return this.orderRows;
    }

    public void setOrderRows(List<PostItem> orderRows){
        this.orderRows = orderRows;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", orderNumber='" + orderNumber + '\'' +
                ", orderRows=" + orderRows +
                '}';
    }
}
