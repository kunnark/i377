package model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PostItem {

    @NotNull
    @Size(min=3, max=500)
    private String itemName;

    @NotNull
    @Min(1)
    private Integer quantity;

    @NotNull
    @Min(1)
    private Integer price;

    public PostItem(){}

    public String getItemName() { return this.itemName; }
    public Integer getQuantity() {
        return this.quantity;
    }
    public Integer getPrice() {
        return this.price;
    }


    public void setItemName(String itemName) { this.itemName = itemName; }
    public void setQuantity(Integer quantity) { this.quantity = quantity; }
    public void setPrice(Integer price) { this.price = price; }


    @Override
    public String toString() {
        return "PostItem{" +
                "itemName='" + itemName + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
