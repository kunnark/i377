package controller;

import dao.PostDao;
import model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private PostDao dao;

    @GetMapping(value = "orders", produces = "application/json; charset=UTF-8")
    public List<Post> getPosts() {
        return dao.getOrderList();
    }

    @GetMapping(value = "orders/{orderId}", produces = "application/json; charset=UTF-8")
    public Post getOrderById(@PathVariable("orderId") Long orderId) {
        return dao.getOrderById(orderId);
    }

    @PostMapping(value = "orders", produces = "application/json; charset=UTF-8")
    public Post save(@RequestBody @Valid Post post) {
        return dao.save(post);
    }

    @DeleteMapping(value = "posts/{postId}", produces = "application/json; charset=UTF-8")
    public void deletePost(@PathVariable("postId") Long postId) {
        dao.delete(postId);
    }

}