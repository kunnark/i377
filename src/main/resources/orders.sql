
INSERT INTO POSTITEM (id, itemName, quantity, price, postID)
  VALUES (NEXT VALUE FOR seq1, 'A123', 20, 100, 1);

INSERT INTO POSTITEM (id, itemName, quantity, price, postID)
  VALUES (NEXT VALUE FOR seq1, 'A456', 21, 200, 1);

INSERT INTO POSTITEM (id, itemName, quantity, price, postID)
  VALUES (NEXT VALUE FOR seq1, 'A457', 21, 1000, 1);

INSERT INTO POST (id, orderNumber)
    VALUES (NEXT VALUE FOR seq2, 'A3');