DROP SCHEMA public CASCADE;

CREATE SEQUENCE seq1 START WITH 1;

CREATE TABLE POSTITEM (
   id BIGINT NOT NULL PRIMARY KEY,
   itemName varchar(255),
   quantity int,
   price int,
   postID bigint
);

CREATE SEQUENCE seq2 START WITH 1;
create table POST(
    id BIGINT not null primary key,
    orderNumber varchar(255)
);
